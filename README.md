# Eventtus Code Challenge #

First of all, I thank the God that helped me to finish the task.

This task wasn't the complex one that I've faced 
but I loved it and enjoyed a lot by working on it
so thank you for giving me the chance.

I wished to implement the extra (bonus) features 
but unfortunatley don't have enough time. 

## Requirements #

- Xcode Version 7.3 (release one).
- Deploy on iOS 9.3.

**Language**
- Swift 2

## How to run:

- First open Tw3pp project using Xcode 7.3 (release one) IDE.
- Link "Tw3pp-Bridging-Header.h" file in Target Build Settings
  in "Objective-C Bridging Header" Section for the app settings.
- Run the app (Command+R) ^_^.

## Overview ##

### Backend ###
- I used Fabric , TwitterKit and TwitterCore frameworks.
- Used a third-party library that called “JSONModel”
  to take the advantage of modelling the data models
  and the connection requests/responses models. 

### Design ###
- Support (all) iPhone/iPad devices.
- Used the Auto-layout techniques to make the app
  compatible with all Apple iPhones/iPads devices
  in all orientations. 

### Others ###
- Used SWWebImage third party lib to cache images.
- App caches Get Followers api response to be used
  in offline mode.

Source Control
- Git.

## Important Note ##

--TODO

#Author
------------------------------------------------------------
Name          Ahmad Abdul Gawad Mahmoud
Email         ahmad_abdulgawad.mahmoud@yahoo.com
LinkedIn      https://linkedin.com/in/ahmadabdulgawadmahmoud