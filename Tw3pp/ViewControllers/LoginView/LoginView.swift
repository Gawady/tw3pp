//
//  LoginView.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/22/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit
import TwitterKit


class LoginView: UIViewController {
    
    
    // MARK:- PROPERTIES
    
    @IBOutlet weak var buttonTWLogin: UIButton!
    
    //MARK:- VIEW METHODS

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        preSettings()
    }
    
    func preSettings() {
        
        // add twitter login btn,
        
        twitterLogIn()
    }
    
    func SaveUserInPreferences (userObj: User?) {
        
        // keep user data in app preferences
        
        NSUserDefaults.standardUserDefaults().setValue(userObj?.toDictionary(), forKey: "user")
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "UserLogged")
        
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func navigateToFollowersPage () {
        
        // navigate to followers page
        
        self.performSegueWithIdentifier("goToFollowersView", sender: self)
    }
    
    //MARK:- TWITTER
    
    func twitterLogIn() {
        
        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
            
            
            if (session != nil) {
                
                let user = User()
                user.id = Int(session!.userID)
                user.screen_name = session!.userName

                // save loggedin user in preferences.
                
                self.SaveUserInPreferences(user)
                
                
                // navigate user to followers page
                
                self.performSelector(#selector(LoginView.navigateToFollowersPage), withObject: nil, afterDelay: 1)
                
            } else {
                
                 Utils.showAlertDialogInView(withTilte: "Info", andMessage: error!.localizedDescription, andButtonTitle: "OK")
            }
            
        })
        
        logInButton.center = self.view.center
        
        // hide twitter aweful one,
        
        logInButton.hidden = true
        
        // Add action for my customised button.
        
        let loginViewAction = logInButton.actionsForTarget(logInButton, forControlEvent: .TouchUpInside)?.first
        
        buttonTWLogin.addTarget(logInButton, action: NSSelectorFromString(loginViewAction!), forControlEvents: .TouchUpInside)

        
        self.view.addSubview(logInButton)
    }
}
