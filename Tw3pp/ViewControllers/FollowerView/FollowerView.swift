//
//  FollowerView.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/22/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

import TwitterKit

class FollowerView: BaseViewController, TWTRTweetViewDelegate {
    
    // MARK:- VARIABLES
    
    var selectedUser: User?
    
    var prototypeCell: TWTRTweetTableViewCell?
    
    let tweetTableCellReuseIdentifier = "TweetCell"
    
    var isLoadingTweets = false
    
    var tweetIDs = [String]()
    
    // setup a 'container' for Tweets
    
    var tweets: [TWTRTweet] = [] {
        
        didSet {
            
            tableViewLastTweets.reloadData()
        }
    }
    
    
    // MARK:- PROPERTIES
    
    @IBOutlet weak var activityIndTweets: UIActivityIndicatorView!
    
    @IBOutlet weak var tableViewLastTweets: UITableView!
    
    
    // MARK:- VIEW METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set up UI
        
        preSettings()
    }
    
    
    
    func preSettings() {
        
        // set nav title
        
        let selectedUserScreenName = selectedUser?.screen_name
        
        self.navigationItem.title = "👤 @\(selectedUserScreenName!)"
        
        // make connection to get Top Ten Tweets For CurrentUser
        
        getTopTenTweetsForCurrentUser()
    }
    
    
    func getTopTenTweetsForCurrentUser() {
        
        // fetch saved data for logged user,
        
        let userId = Int((selectedUser?.id)!)
        
        let userScreenName = selectedUser?.screen_name
        
        // start animating the indicator, then make connection to get followers,
        
        activityIndTweets.startAnimating()
        
        // more info for this api :: https://dev.twitter.com/rest/reference/get/followers/list
        
        server!.GetTopTenTweets(forUserId: userId, andScreenName: userScreenName, andCount: 10, onDelegate: self)
    }
    
    func prepareLoadingTweets() {
        
        // Create a single prototype cell for height calculations.
        
        self.prototypeCell = TWTRTweetTableViewCell(style: .Default, reuseIdentifier: tweetTableCellReuseIdentifier)
        
        // Register the identifier for TWTRTweetTableViewCell.
        
        self.tableViewLastTweets.registerClass(TWTRTweetTableViewCell.self, forCellReuseIdentifier: tweetTableCellReuseIdentifier)
        
        // Setup table data
        
        loadTweets()
    }
    
    func loadTweets() {
        
        // Do not trigger another request if one is already in progress.
        
        if self.isLoadingTweets {
            
            return
        }
        
        self.isLoadingTweets = true
        
        // set tweetIds to find
        
        let client = TWTRAPIClient.clientWithCurrentUser()
        
        client.loadTweetsWithIDs(tweetIDs, completion: { tweet, error in
            
            if let t = tweet {
                
                // If there are tweets do something magical
                if ((t.count) > 0) {
                    
                    // Loop through tweets and do something
                    for i in t {
                        // Append the Tweet to the Tweets to display in the table view.
                        self.tweets.append(i)
                    }
                } else {
                    
                    Utils.showAlertDialogInView(withTilte: "Info", andMessage: "Failed to load Tweet: \(error!.localizedDescription)", andButtonTitle: "OK")
                }
            }
        })
        
    }
    
    func refreshInvoked() {
        
        // Trigger a load for the most recent Tweets.
        
        loadTweets()
    }
    
    // MARK: TWTRTweetViewDelegate
    
    func tweetView(tweetView: TWTRTweetView!, didSelectTweet tweet: TWTRTweet!) {
        
        // Display a Web View when selecting the Tweet.
        
        let webViewController = UIViewController()
        
        let webView = UIWebView(frame: webViewController.view.bounds)
        
        webView.loadRequest(NSURLRequest(URL: tweet.permalink))
        
        webViewController.view = webView
        
        self.navigationController?.pushViewController(webViewController, animated: true)
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Return the number of Tweets.
        
        return tweets.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Retrieve the Tweet cell.
        
        let cell = tableView.dequeueReusableCellWithIdentifier(tweetTableCellReuseIdentifier, forIndexPath: indexPath) as! TWTRTweetTableViewCell
        
        // Assign the delegate to control events on Tweets.
        
        cell.tweetView.delegate = self
        
        // Retrieve the Tweet model from loaded Tweets.
        let tweet = tweets[indexPath.row]
        
        // Configure the cell with the Tweet.
        
        cell.configureWithTweet(tweet)
        
        cell.tweetView.showActionButtons = true
        
        // Set the theme directly
        
        cell.tweetView.theme = .Dark
        
        // Return the Tweet cell.
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let tweet = self.tweets[indexPath.row]
        
        self.prototypeCell?.configureWithTweet(tweet)
        
        let height = TWTRTweetTableViewCell.heightForTweet(tweet, style: .Compact, width: self.view.bounds.width, showingActions: true)
        
        return height
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerVC = self.storyboard?.instantiateViewControllerWithIdentifier("HeaderView") as? HeaderView
        
        // pass selected User
        
        headerVC!.selectedUser = selectedUser
        
        headerVC!.view.frame = CGRectMake(0, 0, tableViewLastTweets.frame.width, 240)
        
        self.tableViewLastTweets.tableHeaderView = headerVC?.view
        
        return headerVC?.view
        
    }
    
    
    //MARK:- CONNECTIONS
    
    func successResponseGetTopTenTweetsApi(withTweetsIds tweets_Ids: [Tweet]?) {
        
        // hide indicator.
        
        activityIndTweets.stopAnimating()
        
        // append the new returned folllowers to arrayFollowers array,
        
        var tmpArr = [String]()
        
        for tweet in tweets_Ids! {
            
            tmpArr.append(String(tweet.id))
        }
        
        if !tmpArr.isEmpty {
            
            tweetIDs.appendContentsOf(tmpArr)
            
            prepareLoadingTweets()
        }
        else {
            
            self.view.makeToast("No tweets found.")
        }
        
        
        
    }
    
    func failResponseGetTopTenTweetsApi(withMessage message: String?) {
        
        //notify user
        
        Utils.showAlertDialogInView(self, withTilte: "Info", andMessage: message, andButtonTitle: "OK")
        
        // hide indicator
        
        activityIndTweets.stopAnimating()
    }
    
    
    
}
