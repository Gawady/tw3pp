//
//  HeaderView.swift
//  Tw3pp
//
//  Created by AlNapedhTech on 6/26/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

class HeaderView: BaseViewController {

    // MARK:- VARIABLES
    
    var selectedUser: User?
    
    // MARK:- PROPERTIES
    
    @IBOutlet weak var imgViewBackground: UIImageView!
    
    @IBOutlet weak var imgViewProfile: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelScreenName: UILabel!
    
    
    // MARK:- VIEW METHODS
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // set up UI
        
        preSettings()
    }
    
    func preSettings() {
        
        // make profile pic rounded
        
        Utils.roundThisControl(self.imgViewProfile)
        imgViewProfile.layer.cornerRadius = 10
        imgViewProfile.layer.borderWidth = 4
        imgViewProfile.layer.borderColor = UIColor.whiteColor().CGColor
        
        // fill data
        
        fillData()
    }
    
    func fillData () {
        
        if selectedUser != nil {
            
            // follower background image
            
            if selectedUser!.profile_banner_url != nil  && selectedUser?.profile_image_url_https != nil {
                
                let imageURL = NSURL(string: selectedUser!.profile_banner_url)
                imgViewBackground.setImageWithURL(imageURL, placeholderImage: UIImage(named: "placeholder"), usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            }
            else {
                
                imgViewBackground.image = UIImage(named: "defaultBG.png")
            }
            
            if !selectedUser!.profile_image_url_https.isEmpty && selectedUser?.profile_image_url_https != nil {
                
                // follower profile image
                
                let imageURL = NSURL(string: selectedUser!.profile_image_url_https)
                imgViewProfile.setImageWithURL(imageURL, placeholderImage: UIImage(named: "placeholder"), usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            }
            else {
                
                imgViewBackground.image = UIImage(named: "placeholder.png")
            }
            
            // follower name
            
            labelName.text = selectedUser!.name
            
            // follower screen name
            
            labelScreenName.text = "@" + selectedUser!.screen_name
        }
    }


}
