//
//  BaseViewController.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, RequestDelegate {

    var server: Server? = Server()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setNavigationBarImage()

    }
    
    func setNavigationBarImage () {

        // background
        
        let img = UIImage(named: "twDarkThemeBG")
        
        self.navigationController?.navigationBar.setBackgroundImage(img, forBarMetrics: .Default)
        
        // tint
        
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()



    }


}
