//
//  SplashView.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/25/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

class SplashView: BaseViewController {
    
    // UI controls
    
    @IBOutlet var progressBar: UIProgressView!
    
    // refer to page control index
    
    var currentProgressValue: Float = 0.0
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // initilization method
        
        preSettings()
    }
    
    func preSettings () {
        
        // check internet connection.
        
        checkInternetStatus()
    }
    
    func checkInternetStatus() {
        
        // hide this control temporarily
        
        self.progressBar.hidden = true
        
        
        if !Utils.isConnectedToNetwork() {
            
            if !Utils.checkCachedDataExist() {
                
                Utils.utils.connectionState = "OffLineNotCached"
                
                self.fireTimer()
                
            } else {
                
                Utils.utils.connectionState = "OffLineCached"
                
                PXAlertView.showAlertWithTitle("Info 😃", message: "You haven't internet connection but the app found data cached before. :)", cancelTitle: "OK ☑️", completion: {(cancelled: Bool, buttonIndex: Int) -> Void in
                    
                    if cancelled {
                        
                        // fire a timer to animate progrss bar.
                        
                        self.fireTimer()
                    }
                })
                
            }
            
        } else {
            
            Utils.utils.connectionState = "OnLine"
            
            // fire a timer to animate progrss bar.
            
            self.fireTimer()
        }
        
        
    }
    
    
    func fireTimer() {
        
        self.progressBar.hidden = false
        
        // set timer to animate page control
        NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(SplashView.UpdateLoading), userInfo: nil, repeats: true)
    }
    
    func UpdateLoading () {
        
        self.currentProgressValue += 0.2
        
        
        UIView.animateWithDuration(1, animations: {() -> Void in
            
            self.progressBar.setProgress(self.currentProgressValue, animated: true)
            
        })
        
        if self.currentProgressValue == 1 {
            
            self.checkUserLogged()
        }
    }
    
    func checkUserLogged () {
        
        // APP PREFERENCES
        
        let loggedInUser = NSUserDefaults.standardUserDefaults().valueForKey("user")
        let userIsLogged = NSUserDefaults.standardUserDefaults().boolForKey("UserLogged")
        
        if loggedInUser != nil &&  userIsLogged == true  {
            
            // navigate to followers page
            
            self.performSegueWithIdentifier("goToFollowersView", sender: self)
        } else {
            
            // present sign in page
            
            let loginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginView") as? LoginView
            
            self.presentViewController(loginVC!, animated: true, completion: nil)
        }
    }
    
}
