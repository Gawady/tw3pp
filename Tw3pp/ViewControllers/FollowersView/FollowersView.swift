//
//  FollowersView.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/22/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

class FollowersView: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK:- VARIABLES
    
    let userLogged = NSUserDefaults.standardUserDefaults().valueForKey(USER)
    
    var arrayFollowers = [User]()
    
    let threshold = 100.0       // threshold from bottom of table view
    
    var isLoadingMore = false   // flag
    
    // refer to page number, -1 refers to the first page

    var currentPage = -1
    
    // MARK:- PROPERTIES
    
    @IBOutlet weak var activityIndFollowers: UIActivityIndicatorView!

    @IBOutlet weak var tableViewFollowers: UITableView!
    
    // MARK:- VIEW METHODS

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // set up UI
        
        preSettings()
    }
    
    func preSettings() {
        
        // set nav title
        
        let loggedScreenName = userLogged?.valueForKey(SCREEN_NAME) as! String
        
        self.navigationItem.title = "👥 @\(loggedScreenName)"
        
        // tell table view to consider autolayout constarints
        
        configureTableView()
    }
    
    func configureTableView() {
        
        tableViewFollowers.rowHeight = UITableViewAutomaticDimension
        
        // default value 
        
        tableViewFollowers.estimatedRowHeight = 83
        
        let imgViewBG = UIImageView(frame: self.tableViewFollowers.frame)
        
        imgViewBG.image = UIImage(named: "twDarkThemeBG")
        
        self.tableViewFollowers.backgroundView = imgViewBG
    }
    
    func getNewFollowers() {
        
        if Utils.utils.connectionState == "OnLine" {
            
            // fetch saved data for logged user,
            
            let userId = userLogged?.valueForKey(ID) as! Int
            
            let userScreenName = userLogged?.valueForKey(SCREEN_NAME) as! String
            
            // start animating the indicator, then make connection to get followers,
            
            activityIndFollowers.startAnimating()
            
            // more info for this api :: https://dev.twitter.com/rest/reference/get/followers/list
            
            server!.GetFollowers(forUserId: userId, andScreenName: userScreenName, andCursor: currentPage, andCount: 20, onDelegate: self)
        }
        else if Utils.utils.connectionState == "OffLineCached" {
            
            
            server!.requestCachedFollowers(onDelegate: self)
        }
        else {
            
            // offline and not cached.
            
            Utils.showAlertDialogInView(withTilte: "Warning ⚠️", andMessage: "You haven't internet connection. No cached data found so try to check your internet connection!", andButtonTitle: "OK ☑️")

        }


    }
    
    //MARK:- collection view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayFollowers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableViewFollowers.dequeueReusableCellWithIdentifier("FollowersCell", forIndexPath: indexPath) as! FollowersCell
        
        // fetch the current follower
        
        let currentUser: User? = arrayFollowers[indexPath.item]
        
        if currentUser != nil {
            
            // follower image
            
            let imageURL = NSURL(string: currentUser!.profile_image_url_https)
            cell.imgViewFollower.setImageWithURL(imageURL, placeholderImage: UIImage(named: "placeholder"), usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            
            // round user image
            
            Utils.roundThisControl(cell.imgViewFollower)
            
            cell.imgViewFollower.layer.borderWidth = 5
            
            cell.imgViewFollower.layer.borderColor = UIColor.whiteColor().CGColor
            
            cell.imgViewFollower.layer.cornerRadius = cell.imgViewFollower.frame.height / 2
            
            // follower name
            
            cell.labelName.text = currentUser!.name
            
            // follower screen name
            
            cell.labelScreenName.text = "@" + currentUser!.screen_name
            
            // follower bio
            
            cell.labelDescription.text = currentUser!.description
            
            // selection color
            
            let backgroundView = UIView()
            
            backgroundView.backgroundColor = UIColor.blackColor()
            
            cell.selectedBackgroundView = backgroundView
        }
        
        return cell

    }
    
    
    
    //MARK:- table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // navigate to FollowerView page
        
        let followerVC = self.storyboard?.instantiateViewControllerWithIdentifier("FollowerView") as? FollowerView
        
        // pass selected user obj.
        
        followerVC!.selectedUser = arrayFollowers[indexPath.row]
        
        self.navigationController?.pushViewController(followerVC!, animated: true)
    }
    
    
    
    //MARK:- CONNECTIONS
    
    func successResponseGetFollowersApi(withFollowers followers: [User]?, andNextCursor next_cursor: NSNumber?) {
        
        // hide indicator.
        
        activityIndFollowers.stopAnimating()
        
        // append the new returned folllowers to arrayFollowers array,
        
        arrayFollowers.appendContentsOf(followers!)
        
        // refresh page
        
        tableViewFollowers.reloadData()
        
        // update current page
        
        if followers?.count > 0 {
            
            if Int(next_cursor!) > 0 && currentPage != Int(next_cursor!) {
                
                // to allow loading new items in future
                
                self.isLoadingMore = false
                
                currentPage = Int(next_cursor!)
            }
            else {
                
                // no new items found so stop loading new items in future
                
                self.isLoadingMore = true
            }
        }
    }
    
    func failResponseGetFollowersApi(withMessage message: String?) {
        
        //notify user
        
        Utils.showAlertDialogInView(self, withTilte: "Info", andMessage: message, andButtonTitle: "OK")
        
        // hide indicator
        
        activityIndFollowers.stopAnimating()
    }
    
    //MARK:- SCROLL VIEW DELEGATE
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        if !isLoadingMore && (Double(maximumOffset) - Double(contentOffset) <= threshold) {
            
            // Get more data - API call
            
            self.isLoadingMore = true
            
            // make connection to get new followers
            
            getNewFollowers()
            
        }
    }

}
