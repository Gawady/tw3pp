//
//  FollowersCell.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/25/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

class FollowersCell: UITableViewCell {
    
    @IBOutlet var imgViewFollower: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelScreenName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
}
