//
//  Server.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

@objc protocol RequestDelegate {
    
    // Get Followers delegate methods
    
    optional func successResponseGetFollowersApi(withFollowers followers: [User]?, andNextCursor next_cursor: NSNumber?)
    optional func failResponseGetFollowersApi(withMessage message: String?)
    
    // Get Top Ten Tweets delegate methods
    
    optional func successResponseGetTopTenTweetsApi(withTweetsIds tweetsIds: [Tweet]?)
    optional func failResponseGetTopTenTweetsApi(withMessage message: String?)
    
}


class Server: NSObject {

    //MARK:- get followers connection
    
    func GetFollowers(forUserId user_id: Int?, andScreenName screen_name: String?, andCursor cursor: Int?, andCount count: Int?, onDelegate delegate: AnyObject?) {
        
        let getfollowersRequest = GetFollowersRequest()
        getfollowersRequest.user_id = user_id
        getfollowersRequest.screen_name = screen_name
        getfollowersRequest.cursor = cursor
        getfollowersRequest.count = count
        
        
        RequestConnection.makeRequestWithApiName(API_GET_FOLLOWERS_URL, andResponseClass: GetFollowersResponse.classForCoder(), andParams: getfollowersRequest, withHandler: {(response: AnyObject!,apiName: String!,error: String?) -> Void in
            
            if (apiName == API_GET_FOLLOWERS_URL) {
                
                if response != nil {
                    
                    let jsonResponse = response as! GetFollowersResponse
                    
                    if ((error ) == nil) {
                        
                        // check if cached data not found and else save new one.
                        
                        self.checkCachedData(response.toDictionary())
                        
                        let followersArr = jsonResponse.users as? [User]
                        
                        delegate?.successResponseGetFollowersApi!(withFollowers: followersArr, andNextCursor: jsonResponse.next_cursor)
                    } else {
                        
                        delegate?.failResponseGetFollowersApi!(withMessage: error)
                    }
                    
                } else {
                    delegate?.failResponseGetFollowersApi!(withMessage: error)
                }
            }
        })
    }
    
    func checkCachedData(returnedResponse: NSDictionary) {
        
        // check if cachedResponseGetFollowersApi.plist found.
        
        if !Utils.checkCachedDataExist() {
            
            // so write the returned response to cachedResponseGetFollowersApi.plist file
            
            Utils.writeToFileThisDictionary(returnedResponse)
        }
        
    }
    
    func requestCachedFollowers(onDelegate delegate: AnyObject?) {
        
        let cachedDataFilePath = Utils.getCachedFilePath()
        
        let cachedDataDictionary = NSMutableDictionary(contentsOfFile: cachedDataFilePath)
        
        print("cachedResponseGetFollowersApi.plist file loaded.")
        
        encapsulateCachedData(cachedDataDictionary, onDelegate: delegate)
    }
    
    func encapsulateCachedData(cachedDataDictionary: NSDictionary?, onDelegate delegate: AnyObject?) {
        
        // parse here..
        
        RequestConnection.encapsulateCachedDataWithThisResponse(cachedDataDictionary, andResponseClass: GetFollowersResponse.classForCoder(), withHandler: {(response: AnyObject!, error: String?) -> Void in
            
            if response != nil {
                
                let jsonResponse = response as! GetFollowersResponse
                
                if ((error ) == nil) {
                    
                    let followersArr = jsonResponse.users as? [User]
                    
                    delegate?.successResponseGetFollowersApi!(withFollowers: followersArr, andNextCursor: jsonResponse.next_cursor)
                    
                } else {
                    
                    delegate?.failResponseGetFollowersApi!(withMessage: error)
                }
                
            } else {
                
                delegate?.failResponseGetFollowersApi!(withMessage: error)
            }
        })
    }
    
    
    //MARK:- get top ten tweets connection
    
    func GetTopTenTweets(forUserId user_id: Int?, andScreenName screen_name: String?, andCount count: Int?, onDelegate delegate: AnyObject?) {
        
        let getTopTenTweetsRequest = BaseModel()
        getTopTenTweetsRequest.user_id = user_id
        getTopTenTweetsRequest.screen_name = screen_name
        getTopTenTweetsRequest.count = count
        
        RequestConnection.makeRequestWithApiName(API_GET_TOP_TEN_TWEETS_URL, andResponseClass: GetTopTenTweetsResponse.classForCoder(), andParams: getTopTenTweetsRequest, withHandler: {(response: AnyObject!,apiName: String!,error: String?) -> Void in
            
            if (apiName == API_GET_TOP_TEN_TWEETS_URL) {
                
                if response != nil {
                    
                    let jsonResponse = response as! GetTopTenTweetsResponse
                    
                    if ((error ) == nil) {
                        
                        let tweetsIdsArr = jsonResponse.tweets as? [Tweet]
                        
                        delegate?.successResponseGetTopTenTweetsApi!(withTweetsIds: tweetsIdsArr)
                    } else {
                        
                        delegate?.failResponseGetTopTenTweetsApi!(withMessage: error)
                    }
                    
                } else {
                    delegate?.failResponseGetTopTenTweetsApi!(withMessage: error)
                }
            }
        })
    }
}
