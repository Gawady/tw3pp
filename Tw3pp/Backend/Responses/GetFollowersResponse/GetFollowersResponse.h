//
//  GetFollowersResponse.h
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

#import "BaseModel.h"
#import "User.h"

@interface GetFollowersResponse : BaseModel

@property NSArray <User, Optional> *users;

@end
