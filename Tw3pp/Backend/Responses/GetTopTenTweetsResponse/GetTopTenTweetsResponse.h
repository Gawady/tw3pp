//
//  GetTopTenTweetsResponse.h
//  Tw3pp
//
//  Created by AlNapedhTech on 6/26/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

#import "BaseModel.h"
#import "Tweet.h"

@interface GetTopTenTweetsResponse : BaseModel

@property NSArray <Tweet, Optional> *tweets;


@end
