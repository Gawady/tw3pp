//
//  RequestConnection.m
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

#import "RequestConnection.h"
#import <TwitterKit/TwitterKit.h>

#define API_GET_FOLLOWERS_URL @"https://api.twitter.com/1.1/followers/list.json"

#define API_GET_TOP_TEN_TWEETS_URL @"https://api.twitter.com/1.1/statuses/user_timeline.json"

@implementation RequestConnection

+(void) makeRequestWithApiName:(NSString *)apiURL andResponseClass:(id)responseClass andParams:(BaseModel*)params withHandler:(void (^)(id, NSString*, NSString*))completionHandler
{
    
    // Objective-C
    
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
    
    NSString *url = apiURL;
    
    NSDictionary *requestDic;

    requestDic = [params toDictionary];
    
    // build request url..
    
    NSString *tmpStr = [RequestConnection convertToUrlParametersForm:requestDic];
    
    url = [url stringByAppendingString:[NSString stringWithFormat:@"?%@", tmpStr]];

    NSError *clientError;
    
    NSURLRequest *request = [client URLRequestWithMethod:@"GET" URL:url parameters:nil error:&clientError];

    
    NSLog(@"URL: %@", url);
    
    if (request) {
        
        
        [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            if (data) {
                
                // handle the response data e.g.
                
                NSError *jsonError;
                
                // repair Response for GetTopTweetsApi
                
                if ([apiURL isEqualToString:API_GET_TOP_TEN_TWEETS_URL]) {
                    
                    NSMutableString* newStr = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    NSString *tmp = [NSString stringWithFormat:@"{\"tweets\": %@ }", newStr];
                    
                    data = [tmp dataUsingEncoding:NSUTF8StringEncoding];
                    
                    
                    NSLog(@" ");
                }
                
                
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                data = [[responseClass alloc] initWithDictionary:json error:nil];
                
                NSLog(@"response :: %@", json);
                
                if (data) {
                    
                    completionHandler(data, apiURL, nil);
                }
                else {
                    
                    completionHandler(nil, apiURL, [jsonError localizedDescription]);
                }
                
            }
            else {
                completionHandler(nil, apiURL, [connectionError localizedDescription]);
            }
        }];
    }
    else {
        completionHandler(nil, apiURL, [clientError localizedDescription]);
    }
}

+(void) encapsulateCachedDataWithThisResponse:(id)cachedResponse andResponseClass:(id)responseClass withHandler:(void (^)(id, NSString*))completionHandler {
    
    NSError *error;
    
    id encapsulatedResponse;
    
    if (cachedResponse) {
        
        encapsulatedResponse = [[responseClass alloc] initWithDictionary:cachedResponse error:&error];
        
        NSLog(@"Json: %@", encapsulatedResponse);
        
    } else {
        
        NSLog(@"Error: %@",error);
    }
    
    completionHandler(encapsulatedResponse, error.description);
}

+ (NSMutableString*) convertToUrlParametersForm: (NSDictionary*) params {
    
    //create the request body
    NSMutableString* paramsString = nil;
    
    if (params) {
        //build a simple url encoded param string
        paramsString = [NSMutableString stringWithString:@""];
        for (NSString* key in [[params allKeys] sortedArrayUsingSelector:@selector(compare:)]) {
            [paramsString appendFormat:@"%@=%@&", key, [params valueForKey:key]];
        }
        if ([paramsString hasSuffix:@"&"]) {
            paramsString = [[NSMutableString alloc] initWithString: [paramsString substringToIndex: paramsString.length-1]];
        }
    }
    return paramsString;
}

@end
