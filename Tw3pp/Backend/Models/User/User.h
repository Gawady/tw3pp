//
//  User.h
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

#import "BaseModel.h"

@protocol User @end

@interface User : BaseModel

@property NSNumber <Optional> *id;
@property NSString <Optional> *name;
@property NSString <Optional> *description;
@property NSString <Optional> *profile_image_url_https;
@property NSString <Optional> *profile_banner_url;



@end
