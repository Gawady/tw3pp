//
//  BaseModel.h
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

#import "JSONModel.h"

@interface BaseModel : JSONModel

@property NSNumber <Optional> *user_id;
@property NSString <Optional> *screen_name;
@property NSNumber <Optional> *count;
@property NSNumber <Optional> *next_cursor;

@end
