//
//  AppConstants.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import Foundation

// APIs

let API_GET_FOLLOWERS_URL = "https://api.twitter.com/1.1/followers/list.json"

let API_GET_TOP_TEN_TWEETS_URL = "https://api.twitter.com/1.1/statuses/user_timeline.json"


// KEYWORDS

let USER = "user"

let ID = "id"

let SCREEN_NAME = "screen_name"