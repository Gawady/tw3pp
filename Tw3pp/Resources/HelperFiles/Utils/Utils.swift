//
//  Utils.swift
//  Tw3pp
//
//  Created by Ahmad Abdul-Gawad Mahmoud on 6/24/16.
//  Copyright © 2016 Ahmad Abdul-Gawad Mahmoud. All rights reserved.
//

import UIKit

import SystemConfiguration


class Utils: NSObject {

    // this'll be the app singleton object.
    
    static let utils = Utils()
    
    var connectionState: String?
    
    static var mycurrentView: UIViewController?

    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    class func showAlertDialogInView(view: UIViewController,withTilte title: String?, andMessage message: String?, andButtonTitle buttonTitle: String?) {
        
        let alertViewController = UIAlertController(title: title!, message: message!, preferredStyle: .Alert)
        alertViewController.addAction(UIAlertAction(title: buttonTitle, style: .Default, handler: nil))
        view.presentViewController(alertViewController, animated: true, completion: nil)
        
        
    }
    
    class func showAlertDialogInView(withTilte title: String?, andMessage message: String?, andButtonTitle buttonTitle: String?) {
        
        PXAlertView.showAlertWithTitle(title, message: message, cancelTitle: buttonTitle, completion: nil)
    }
    
    
    class func screenSize () -> CGSize {
        
        let screenSize = UIScreen.mainScreen().bounds.size
        if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation)) {
            
            return CGSizeMake(screenSize.height, screenSize.width)
        }
        
        return screenSize
    }
    
    class func roundThisControl (control: UIView) -> UIView
    {
        control.layer.cornerRadius = 5
        control.layer.masksToBounds = true
        
        control.layer.borderWidth = 0.5
        control.layer.borderColor = UIColor.grayColor().CGColor
        return control
    }
    
    class func checkCachedDataExist() -> Bool {
        
        let readFilePath = getCachedFilePath()
        
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(readFilePath as String) {
            print("cachedResponseGetFollowersApi.plist FILE AVAILABLE")
            return true
        } else {
            print("cachedResponseGetFollowersApi.plist FILE NOT AVAILABLE")
            return false
        }
    }
    
    class func getCachedFilePath() -> String {
        
        let dirPath: NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first!
        let filePath = dirPath.stringByAppendingPathComponent("cachedResponseGetFollowersApi.plist")
        
        return filePath
    }
    
    class func writeToFileThisDictionary(dictioanry: NSDictionary) {
        
        if let writefilePath: String = getCachedFilePath() {
            
            dictioanry.writeToFile(writefilePath, atomically: false)
            
            print("cachedResponseGetFollowersApi.plist FILE SAVED.")
            
        } else {
            
            Utils.showAlertDialogInView(withTilte: "Error", andMessage: "Some error occured during storing the cached data.", andButtonTitle: "OK")
            
        }
        
    }
}
